import unittest


class Test(unittest.TestCase):
    def test_a(self):
        self.assertEqual('a', 'a')

    def test_b(self):
        self.assertEqual('b', 'b')

    def test_c(self):
        self.assertEqual('c', 'c')


if __name__ == '__main__':
    unittest.main()
