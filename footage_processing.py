from natsort import natsorted
from os import remove, stat, walk
from pathlib import Path
from prompt import print_highlight, prompt
from subprocess import PIPE, run
from shutil import move
from typing import Optional


def pre_process_footage(path: Path, sound: Path) -> None:
    print('Pre processing footage:', path)
    if not path.is_dir():
        print('Warning: footage not found.', path)
        return

    prev_dirpath = None
    for dirpath, _, files in walk(str(path)):

        remove_audio = None

        for file in natsorted(files):
            f = Path(dirpath, file)
            if f.suffix in ['.mov', '.mpeg', '.mp4']:
                print_highlight("Pre processing movie: {}/".format(str(f.parent)), f.name)

                if prev_dirpath != dirpath:
                    prev_dirpath = dirpath
                    remove_audio = prompt("Remove audio", " from files in {}?".format(dirpath), sound)

                if remove_audio:
                    movie_remove_audio(f)

                size = movie_get_size(f)
                length = movie_get_length(f)
                rate = size / length

                print("Size = {:.2f}mb, duration = {:.2f}sec, rate = {:.2f}mb/sec".format(size, length, rate))

                print("Going to encode with h264...")
                movie_convert_to_h264(f)

                print()
    print()


def process_footage(watermark: Path, path: Path) -> None:
    print('Processing footage:', path)
    if not path.is_dir():
        print('Warning: footage not found.', path)
        return

    for dirpath, _, files in walk(str(path)):
        for file in files:
            f = Path(dirpath, file)
            if f.suffix == '.mov' or f.suffix == '.mpeg':
                print("Processing movie:", f)

                f = movie_scale_down(f)
                movie_add_watermark(watermark, f)
                print()
    print()


def movie_convert_to_h264(f: Path) -> None:
    """
    ffmpeg -y -i input.mov -c:v libx264 -acodec aac -strict -2 -b:a 384k -pix_fmt yuv420p out.mp4
    """
    print("Converting to h264...")
    out = Path(f.parent / (f.stem + '_compressed' + f.suffix))
    command = [
        'ffmpeg', '-y', '-hide_banner', '-loglevel', 'panic', '-i', f,
        '-c:v', 'libx264', "-acodec", 'aac', '-strict', '2', '-b:a', '384k', '-pix_fmt', 'yuv420p', out
    ]
    result = run(command, universal_newlines=True)
    if result.returncode != 0:
        print("Error: could not encode with libx264.")
        return

    size = movie_get_size(out)
    length = movie_get_length(out)
    rate = size / length

    print("Success: libx264 applied. New size = {:.2f}mb, duration = {:.2f}sec, rate = {:.2f}mb/sec".format(size, length, rate))

    move(str(out), str(f))


def movie_remove_audio(f: Path) -> None:
    """
    ffmpeg -y -hide_banner -loglevel panic -i input.mp4 -vcodec copy -an output.mp4
    """
    print("Removing audio...")
    out = f.parent / (f.stem + "_noaudio" + f.suffix)
    command = ['ffmpeg', '-y', '-hide_banner', '-loglevel', 'panic', '-i', f, '-vcodec', 'copy', "-an", out]
    result = run(command, universal_newlines=True)
    if result.returncode != 0:
        print("Error: could not remove audio tracks.")
        return
    print("Success: all audio tracks removed.")
    move(str(out), str(f))


def movie_scale_down(f: Path) -> Optional[Path]:
    """
    ffmpeg -y -i input.mov -vf scale=568x320 output.mp4
    """
    print("Scaling down...")
    out = Path(f.parent / (f.stem + '.mp4'))
    command = ['ffmpeg', '-y', '-i', f, '-vf', "scale=568x320", out]
    result = run(command, stdout=PIPE, stderr=PIPE, universal_newlines=True)
    if result.returncode != 0:
        print("Error: could not generate a movie preview.")
        return None
    print("Success: movie preview generated.")
    remove(str(f))
    return out


def movie_add_watermark(w: Path, f: Path) -> None:
    """
    ffmpeg -y -i input.mp4 -i watermark.png -filter_complex "overlay=(W-w)/2:(H-h)/2" -codec:a copy output.mp4
    """
    print("Adding watermark...")
    out = f.parent / (f.stem + "_watermark" + f.suffix)
    command = ['ffmpeg', '-y', '-i', f, '-i', w, "-filter_complex", "overlay=(W-w)/2:(H-h)/2", out]
    result = run(command, stdout=PIPE, stderr=PIPE, universal_newlines=True)
    if result.returncode != 0:
        print("Error: could not apply watermark.")
        return
    print("Success: watermark applied.")
    move(str(out), str(f))


def movie_get_size(f: Path) -> float:
    st = stat(str(f))
    return float(st.st_size / 1024 / 1024)


def movie_get_length(f: Path) -> float:  # seconds
    """
    ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 input.mov
    """
    command = [
        'ffprobe', '-v', 'error', '-show_entries', "format=duration", "-of", "default=noprint_wrappers=1:nokey=1", f
    ]
    result = run(command, stdout=PIPE, stderr=PIPE, universal_newlines=True)
    if result.returncode != 0:
        return -1
    length_string = result.stdout.replace("\"", "")
    return float(length_string)
