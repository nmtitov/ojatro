from playsound import playsound
from pathlib import Path
from sys import stdout


RED = "\033[1;31m"
GREEN = "\033[0;32m"
RESET = "\033[0;0m"


def prompt(question: str, text: str, sound: Path) -> bool:
    playsound((str(sound)))
    valid = {
        "yes": True,
        "y": True,
        "ye": True,
        "no": False,
        "n": False
    }
    while True:
        stdout.write(RED)
        stdout.write(question)
        stdout.write(RESET)
        stdout.write(text)
        stdout.write(" [y/n] ")

        choice = input().lower()
        if choice in valid:
            return valid[choice]
        else:
            stdout.write("Please respond with 'yes' or 'no' (or 'y' or 'n').\n")


def print_error(string: str) -> None:
    stdout.write(RED)
    stdout.write(string+"\n")
    stdout.write(RESET)


def print_success(string: str) -> None:
    stdout.write(GREEN)
    stdout.write(string+"\n")
    stdout.write(RESET)


def print_highlight(string1: str, string2: str) -> None:
    stdout.write(string1)
    stdout.write(RED)
    stdout.write(string2+"\n")
    stdout.write(RESET)
