# Ojatro processing application

## Basics

Launch Terminal app. By default Terminal starts at home directory of current user. Traditional alias is a `~` symbol.

## Start apps

`ojatro_mp4Proc # enter` launch the mp4 processing app

`ojatro_videoCut [Path to Files to be Cut] [Optional Max Length Param] # enter` launch the video cut app

## Pre-requisites
App is located at `~/dev/ojatro`

Before launch create `~/dev/ojatro/local_settings.py` with following content:

````
upload_path = "/Users/nt/Desktop/upload"
backup_path = "/Users/nt/Desktop/backup_upload"
````

Your paths should obviously differ from mine.

Launch app using `ojatro` command from any location

## Git usage
`Git` is used for version control.

To get latest version of source code open Terminal, then go execute two commands:

````
cd ~/dev/ojatro # press enter
git pull # press enter
````

## Basic navigation:

`cd` gets you back to home directory
`cd ~` gets you back to home directory
`cd -` gets you back to previous terminal location

`cd ~/Desktop` gets you to Desktop
`cd ~/Desktop/upload` gets you to Desktop/upload

`cd ~/dev/ojatro` gets you to the project directory

## Commands used in the app

### Footage processing

Libx264 convertion:

`ffmpeg -y -i input.mov -c:v libx264 -acodec aac -strict -2 -b:a 384k -pix_fmt yuv420p out.mp4`

Remove audio:

`ffmpeg -y -i input.mp4 -vcodec copy -an output.mp4`

Scale down:

`ffmpeg -y -i input.mov -vf scale=568x320 output.mp4`

Watermark for video:

`ffmpeg -y -i input.mp4 -i watermark.png -filter_complex "overlay=(W-w)/2:(H-h)/2" -codec:a copy output.mp4`


Get length:

`ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 input.mov`

### Image processing

Get size:

`magick identify -format "%[fx:w],%[fx:h]" input.jpg`

Scale up:

`magick mogrify -resize 200% input.jpg`

Preview landscape:

`magick mogrify -resize 568x320^ -gravity center -extent 568x320 input.jpg`

Preview portrait:

`magick mogrify -resize 213x320^ -gravity center -extent 213x320 input.jpg`

Watermark image:

`magick composite -gravity center watermark.png input.jpg output.jpg` 
