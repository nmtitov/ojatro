from natsort import natsorted
from os import walk
from pathlib import Path
from subprocess import run, PIPE
from typing import Tuple


def pre_process_images(path: Path) -> None:
    if not path.is_dir():
        print('Warning: images not found:', path)
        return
    print('Pre processing images:', path)

    for dirpath, _, files in walk(str(path)):
        for file in natsorted(files):
            f = Path(dirpath, file)
            if f.suffix == '.jpg':
                print('Pre processing image: {}'.format(f))
                w, h = image_get_size(f)
                if (w < 4000 and h < 6000) or (w < 6000 and h < 4000):
                    image_scale_up(f)
                print()

    print()


def process_images(watermark: Path, watermark_portrait: Path, path: Path) -> None:
    if not path.is_dir():
        print('Warning: images not found:', path)
        return
    print('Processing images:', path)

    for dirpath, _, files in walk(str(path)):
        for file in files:
            f = Path(dirpath, file)
            if f.suffix == '.jpg':
                print('Processing image: {}'.format(f))
                w, h = image_get_size(f)
                if w > h:
                    image_preview_landscape(f)
                    image_add_watermark(watermark, f)
                else:
                    image_preview_portrait(f)
                    image_add_watermark(watermark_portrait, f)
                print()
    print()


def image_get_size(f: Path) -> Tuple[int, int]:
    """
    magick identify -format "%[fx:w],%[fx:h]" input.jpg
    """
    command = ['magick', 'identify', '-format', '%[fx:w],%[fx:h]"', f]
    result = run(command, stdout=PIPE, stderr=PIPE, universal_newlines=True)
    if result.returncode != 0:
        return -1, -1
    size_string = result.stdout.replace("\"", "")
    size_list = size_string.split(",")
    w = int(size_list[0])
    h = int(size_list[1])
    return w, h


def image_scale_up(f: Path) -> None:
    """
    magick mogrify -resize 200% input.jpg
    """
    command = ['magick', 'mogrify', '-resize', '200%', f]
    result = run(command, stdout=PIPE, stderr=PIPE, universal_newlines=True)
    if result.returncode != 0:
        print("Error: could not scale up image.")
        return
    print("Success: scaled up image.")


def image_preview_landscape(f: Path) -> None:
    """
    magick mogrify -resize 568x320^ -gravity center -extent 568x320 input.jpg
    """
    command = ['magick', 'mogrify', '-resize', '568x320^', "-gravity", "center", "-extent", "568x320", f]
    result = run(command, stdout=PIPE, stderr=PIPE, universal_newlines=True)
    if result.returncode != 0:
        print("Error: could not generate a landscape preview.")
        return
    print("Success: landscape preview generated.")


def image_preview_portrait(f: Path) -> None:
    """
    magick mogrify -resize 213x320^ -gravity center -extent 213x320 input.jpg
    """
    command = ['magick', 'mogrify', '-resize', '213x320^', "-gravity", "center", "-extent", "213x320", f]
    result = run(command, stdout=PIPE, stderr=PIPE, universal_newlines=True)
    if result.returncode != 0:
        print("Error: could not generate a portrait preview.")
        return
    print("Success: portrait preview generated.")


def image_add_watermark(w: Path, f: Path) -> None:
    """
    magick composite -gravity center watermark.png input.jpg output.jpg
    """
    command = ['magick', 'composite', '-gravity', 'center', w, f, f]
    result = run(command, stdout=PIPE, stderr=PIPE, universal_newlines=True)
    if result.returncode != 0:
        print("Error: could not apply watermark.")
        return
    print("Success: watermark applied.")
